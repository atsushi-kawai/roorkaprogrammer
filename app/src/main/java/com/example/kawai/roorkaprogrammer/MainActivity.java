package com.example.kawai.roorkaprogrammer;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.Manifest;
import android.view.View;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                                              this,
                                              PERMISSIONS_STORAGE,
                                              REQUEST_EXTERNAL_STORAGE
                                              );
        }

    }

    public interface OpenFileAction {
        java.io.File write(java.io.File file);
        java.io.File append(java.io.File file);
        void read(java.io.File file);
        String toString(java.io.File file);
    }

    public void sendMessage(View view) {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        Log.i("main ", "dir:" + path);
        OpenFileDialog d = new OpenFileDialog(path);
        android.app.AlertDialog.Builder builder = null;
        try {
            builder = d.createOpenFileDialog(this, OpenFileDialog.OpenMode.Read);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        builder.show();
        /*
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        */
    }
}
