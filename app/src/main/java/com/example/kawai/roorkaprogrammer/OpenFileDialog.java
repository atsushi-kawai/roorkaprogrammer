package com.example.kawai.roorkaprogrammer;

import android.content.DialogInterface;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class OpenFileDialog implements OpenFileAction {
    public OpenFileDialog() {
        try {
            this.init("/");
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        path = "/";
            
    }
    public OpenFileDialog(String openDir) {
        try {
            this.init(openDir);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        if (openDir.startsWith("//")) {
            path = openDir.substring(1);
        } else {
            path = openDir;
        }
    }
    public void init(String dirname) throws java.io.IOException{
        java.io.File file= new java.io.File(dirname);
        int counter = 0;
        if (!file.getCanonicalPath().equals("/")) {
            int size = file.list()!=null ? file.list().length:0;
            items = new String[size + 1];
            items[0] = "../";
            if (file.list()!=null) {
                java.lang.System.arraycopy(file.list(),0,items,1,size);
                counter = 1;
            }
        } else {
            items = file.list()!=null?file.list():new String[] {""};
            counter = 0;
        }
        for (;counter<items.length;counter++) {
            java.io.File iItem = new java.io.File(dirname + "/" + items[counter]);
            if (iItem.isDirectory()) {
                items[counter] += "/";
            }
        }
        if (items[0].equals("..//")) {
            items[0] = "../";
        }
        /*
        for (int i = 0; i < items.length; i++) {
            Log.i("OpenFileDialog", "items[" + i + "]:" + items[i]);
        }
        */
    }

    public int move(android.content.Context context,OpenMode mode, String path) {
        OpenFileDialog childopenfiledialog = new OpenFileDialog(path);
        try {
            childopenfiledialog.createOpenFileDialog(context, mode).show();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public java.io.File write(java.io.File file) {
        return file;
    }

    public java.io.File append(java.io.File file) {
        return file;
    }

    public void read(java.io.File file) {
        if (file.isDirectory()) {
            String path = file.getPath();
            String name = file.getName();
            Log.i("read", "path:" + path + "name:" + name);
            if (name.equals("..")) {
                path = path.substring(0, path.length()-3);
                java.io.File p = new File(path);
                path = p.getParent();
            }
            OpenFileDialog dialog = new OpenFileDialog(path);
            try {
                dialog.createOpenFileDialog(mainContext, OpenMode.Read).show();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try{
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String[] linebuf = new String[128];
                int nline;
                String line = reader.readLine();
                for (nline = 0; line != null; nline++) {
                    linebuf[nline] = line;
                    line = reader.readLine();
                }
                reader.close();
                for (int i = 0; i < nline; i++) {
                    Log.i("read", "adr" + i + ":" + linebuf[i]);
                }
            } catch(java.io.IOException e){
                System.out.println(e);
            }
        }
    }

    public String toString() {
        return path;
    }
    
    public enum OpenMode {
        Write,Append,Read
    }
    private String path;
    public String[] items;
    public android.content.Context mainContext;
    
    public android.app.AlertDialog.Builder createOpenFileDialog(final android.content.Context context,final OpenFileDialog.OpenMode mode)
        throws java.io.IOException {
        final java.util.ArrayList<Integer> checkedItems = new java.util.ArrayList<>();
        checkedItems.add(0);
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        mainContext = context;
        builder.setTitle("open_title")
            .setSingleChoiceItems(items, 0, new android.content.DialogInterface.OnClickListener() {
                    @Override
                        public void onClick(android.content.DialogInterface dialog, int which) {
                        checkedItems.clear();
                        checkedItems.add(which);
                    }
                })
            .setPositiveButton("open", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(android.content.DialogInterface dialog, int which) {

                        for (Integer i : checkedItems) {
                            // item_i checked
                            // note: i is undefined when not checked. use switch to do work.

                            // insert / as double / is allowed and no / is error.
                            java.io.File file = new java.io.File(path + "/" + items[i]);
                            switch (mode) {
                              case Write:
                                write(file);
                                break;
                              case Append:
                                append(file);
                                break;
                              case Read:
                                Log.i("read?:", file.getName());
                                read(file);
                                break;
                              default:
                                android.util.Log.d("FileState", toString());
                            }
                        }
                    }
                })
            .setNegativeButton("cancel", null);

        return builder;
    }

}
