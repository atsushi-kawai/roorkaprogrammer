package com.example.kawai.roorkaprogrammer;

public interface OpenFileAction {
    java.io.File write(java.io.File file);
    java.io.File append(java.io.File file);
    void read(java.io.File file);
    String toString();
}
